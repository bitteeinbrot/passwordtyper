#include "PasswordEntryController.h"

void PasswordEntryController::PasswordEntryPut(PasswordEntry* pwEntry, String name, String password) {
	StringHelper::Fill(pwEntry->name, sizeof(pwEntry->name), name);
	StringHelper::Fill(pwEntry->password, sizeof(pwEntry->password), password);
}

bool PasswordEntryController::AddPasswordEntry(PasswordEntry* entry) {
	Options* options = OptionsHelper::GetOptions();
	if (options->passwordEntryCount >= MAX_PASSWORD_ENTRIES) {
		return false;
	}
	int eepromPointer = EEPROM_ADDRESS_PASSWORD_ENTRIES;
	eepromPointer += options->passwordEntryCount * sizeof(PasswordEntry);
	EEPROM.put(eepromPointer, *entry);
	options->passwordEntryCount = options->passwordEntryCount + 1;
	OptionsHelper::SaveOptions(options);
	delete options;
	return true;
}

void PasswordEntryController::SavePasswordEntry(int index, PasswordEntry* entry) {
	int eepromPointer = EEPROM_ADDRESS_PASSWORD_ENTRIES;
	eepromPointer += index * sizeof(PasswordEntry);
	EEPROM.put(eepromPointer, *entry);
}

byte PasswordEntryController::MoveEntry(byte index, int direction) {
	Options* options = OptionsHelper::GetOptions();
	if (index + direction >= 0 && index + direction < options->passwordEntryCount) {
		delete options;
		PasswordEntry* source = GetPasswordEntry(index);
		PasswordEntry* target = GetPasswordEntry(index + direction);
		SavePasswordEntry(index + direction, source);
		SavePasswordEntry(index, target);
		delete source;
		delete target;
		return index + direction;
	}
	delete options;
	return index;
}

void PasswordEntryController::DeletePasswordEntry(int index) {
	Options* options = OptionsHelper::GetOptions();
	int toSwitchCount = options->passwordEntryCount - 1 - index;
	delete options;
	for (int i = 0; i < toSwitchCount; i++) {
		PasswordEntry* tempEntry = GetPasswordEntry(index + i + 1);
		SavePasswordEntry(index + i, tempEntry);
		delete tempEntry;
	}
	options = OptionsHelper::GetOptions();
	options->passwordEntryCount = options->passwordEntryCount - 1;
	OptionsHelper::SaveOptions(options);
	delete options;
}

PasswordEntry* PasswordEntryController::GetPasswordEntry(int index) {
	Options* options = OptionsHelper::GetOptions();
	int passwordEntryCount = options->passwordEntryCount;
	delete options;
	if (index >= 0 && index < passwordEntryCount) {
		int eepromPointer = EEPROM_ADDRESS_PASSWORD_ENTRIES;
		eepromPointer += index * sizeof(PasswordEntry);
		PasswordEntry* tempEntry = new PasswordEntry;
		EEPROM.get(eepromPointer, *tempEntry);
		return tempEntry;
	} else {
		return NULL;
	}
}




