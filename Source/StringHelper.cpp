#include "StringHelper.h"

char* StringHelper::GetCharArrayFromString(String str) {
	char* sign = new char[str.length()+1];
	Fill(sign, str.length()+1, str);
	return sign;
}

char* StringHelper::GetCharArrayWithStop(char theChar) {
	char* sign = new char[2];
	sign[0] = theChar;
	sign[1] = '\0';
	return sign;
}

void StringHelper::Fill(char* target, int targetBufferSize, String value) {
	strncpy(target, value.c_str(), targetBufferSize);
	target[targetBufferSize-1] = '\0';
}

void StringHelper::Initialize(char* string, int bufferLength) {
	for (int i = 0; i < bufferLength; i++) {
		string[i] = '\0';
	}
}

void StringHelper::InsertChar(int index, char* target, int targetBufferLegth, char value) {
	int targetLength = Length(target, targetBufferLegth);
	if (index < 0) {
		if ((index*-1) - 1 <= targetLength) {
			index = targetLength + index + 1;
		} else {
			return;
		}
	}
	if (targetLength + 1 <= targetBufferLegth-1) {
		for (int i = targetLength-1; i >= index; i--) {
			target[i + 1] = target[i];
		}
		target[index] = value;
	}
}

void StringHelper::DeleteChar(int index, char* target, int targetBufferLegth) {
	int targetLength = Length(target, targetBufferLegth);
	if (index < 0) {
		if ((index*-1) - 1 <= targetLength) {
			index = targetLength + index + 1;
		} else {
			return;
		}
	}
	for (int i = index; i < targetBufferLegth-1; i++) {
		target[i] = target[i+1];
	}
}

int StringHelper::Length(char* string, int bufferLength) {
	for (int i = 0; i < bufferLength; i++) {
		if (string[i] == '\0') {
			return i;
		}
	}
	return bufferLength-1;
}

bool StringHelper::Equals(char* string1, char* string2) {
	int i = 0;
	do {
		if (string1[i] != string2[i]) {
			return false;
		}
		if (string1[i] == '\0') {
			return true;
		}
		i++;
	} while(true);
}

