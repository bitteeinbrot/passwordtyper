#include "MainController.h"
#include "PasswordEntryController.h"
#include "Menu.h"
#include "OptionsHelper.h"
#include "SoftwareKeyboard.h"

//int FreeRam () {
//	extern int __heap_start, *__brkval; 
//	int v; 
//	return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
//}

MainController* MainController::_instance = NULL;

MainController* MainController::GetInstance() {
	if (!_instance) {
		_instance = new MainController();
	}
	return _instance;
}

MainController::MainController() {
	/* ------------------------------- START ---------------------------------- */
	/* Initialize buttons. */
	_buttons = new byte[6];
	_buttons[0] = BUTTON_LEFT;
	_buttons[1] = BUTTON_RIGTH;
	_buttons[2] = BUTTON_TOP;
	_buttons[3] = BUTTON_BOTTOM;
	_buttons[4] = BUTTON_BACK;
	_buttons[5] = BUTTON_OK;

	_states = new bool[6];
	_released = new bool[6];

	for (byte i = 0; i < 6; i++) {
		pinMode(_buttons[i], INPUT);
		_states[i] = false;
		_released[i] = false;
	}
	/* -------------------------------- END ----------------------------------- */

	/* ------------------------------- START ---------------------------------- */
	/* Initialize everything to handle modes. */
	_mode = MODE_NONE;
	_subMode = MODE_NONE;
	_modeStack = new byte[5];
	for (byte i = 0; i < 5; i++) {
		_modeStack[i] = 0;
	}
	_modeStackPointer = 0;
	_activeMenu = NULL;
	/* -------------------------------- END ----------------------------------- */

	/* ------------------------------- START ---------------------------------- */
	/* Initialize everything for PIN input. */
	_pinCommand = COMMAND_PIN_START;
	_currentPin = NULL;
	/* -------------------------------- END ----------------------------------- */

	_softwareKeyboard = NULL;
	_currentPasswordEntryIndex = 0;

	PushModeStack(MODE_PIN); // Load first mode.
}

void MainController::Setup(Adafruit_SSD1306* display) {
	_display = display;
}

void MainController::Loop() {
	/* ------------------------------- START ---------------------------------- */
	/* Check button states. */
	for (int i = 0; i < 6; i++) {
		int state = digitalRead(_buttons[i]);
		if (_states[i] == false && state == HIGH) {
			_released[i] = true;
		}
		_states[i] = (bool) state;
	}
	/* -------------------------------- END ----------------------------------- */

	HandleUserInput();
	Render();
	
	/* ------------------------------- START ---------------------------------- */
	/* Reset, to see if button will get released next tick. */
	for (int i = 0; i < 6; i++) {
		_released[i] = false;
	}
	/* -------------------------------- END ----------------------------------- */
}

void MainController::Render() {
	_display->clearDisplay();
    _display->setTextSize(1);
	_display->setTextColor(WHITE);

	switch(_mode) {
	case MODE_KEYBOARD:
		{
			_softwareKeyboard->Render(_display);
		}
		break;
	case MODE_PASSWORD_ENTRIES:
		{
			_menuPasswordEntries->Render(_display);
			_menuMain->Render(_display);
			_display->drawLine(22, 0, 22, 64, WHITE);
		}
		break;
	case MODE_PASSWORD_ENTRY_DETAIL:
		{		
			PasswordEntry* currentPasswordEntry = PasswordEntryController::GetPasswordEntry(_currentPasswordEntryIndex);
			_menuPasswordEntryDetail->Render(_display);
			_display->setCursor(0, 0);
			_display->print(currentPasswordEntry->name);
			delete currentPasswordEntry;

			Options* options = OptionsHelper::GetOptions();
			_display->setCursor(0, 15);
			_display->print(F("Pos: ("));
			_display->print((_currentPasswordEntryIndex+1));
			_display->print(F("/"));
			_display->print(options->passwordEntryCount);
			_display->print(F(")"));
			delete options;
		}
		break;
	case MODE_PIN:
		{
			if (_pinCommand == COMMAND_PIN_START) {
				_display->setCursor(38, 17);
				_display->print(F("Enter PIN"));
			} else {
				_display->setCursor(42, 17);
				_display->print(F("New PIN"));
			}

			Options* options = OptionsHelper::GetOptions();
			int pinLength = sizeof(options->pin) - 1;
			delete options;
			uint8_t xOffset = 38;
			uint8_t yOffset = 32;
			for (int i = 0; i < pinLength; i++) {
				_display->setCursor(xOffset + 9*i + 1, yOffset);
				_display->print(_currentPin[i]);
				_display->drawRect(xOffset + 9*i, yOffset+9, 7, 1, WHITE);
			}
		}
		break;
	case MODE_OPTIONS:
		{
			_menuOptions->Render(_display);
		}
		break;
	}

	_display->display();
}

MenuItem* MainController::GetMenuItem(int8_t index, int8_t menuType) {
	switch(menuType) {
	case Menu::MENU_TYPE_PASSWORD_ENTRIES:
		{
			MenuItem* tempItem = new MenuItem;
			tempItem->command = COMMAND_BY_POINTER_VALUE;
			PasswordEntry* entry = PasswordEntryController::GetPasswordEntry(index);
			if (entry != NULL) {
				int len = StringHelper::Length(entry->name, sizeof(entry->name)) + 1;
				char* tempName = new char[len];
				StringHelper::Initialize(tempName, len);
				StringHelper::Fill(tempName, len, entry->name);
				tempItem->displayValue = tempName;
				delete entry;
			}
			return tempItem;
		}
		break;
	case Menu::MENU_TYPE_KEYBOARD_SPECIALS:
		{
			MenuItem* tempItem = new MenuItem;
			for (int i = 0; i < SPECIAL_CHARS_SIZE; i++) {
				if (i == index) {
					char tempChar = pgm_read_byte(&SPECIAL_CHARS[i]);
					tempItem->displayValue = StringHelper::GetCharArrayWithStop(tempChar);
					tempItem->command = COMMAND_KEYBOARD_ADD_CHAR;
					return tempItem;
				}
			}
			return tempItem;
		}
		break;
	}
}

void MainController::PushModeStack(int8_t mode) {
	if (mode != MODE_NONE) {
		_modeStack[_modeStackPointer] = mode;
		_modeStackPointer = _modeStackPointer + 1;
		SwitchMode(mode);
	}
}

void MainController::PopModeStack() {
	if (_modeStackPointer > 1) {
		_modeStackPointer = _modeStackPointer - 1;
		SwitchMode(_modeStack[_modeStackPointer - 1]);
	}
}

void MainController::SwitchMode(byte mode) {
	/* Deleted everything that was needed for the old mode. */
	switch(_mode) {
	case MODE_KEYBOARD:
		delete _softwareKeyboard;
		_activeMenu = NULL;
		break;
	case MODE_PASSWORD_ENTRIES:
		delete _menuPasswordEntries;
		_menuPasswordEntries = NULL;
		delete _menuMain;
		_menuMain = NULL;
		_activeMenu = NULL;
		break;
	case MODE_PASSWORD_ENTRY_DETAIL:
		delete _menuPasswordEntryDetail;
		_menuPasswordEntryDetail = NULL;
		_activeMenu = NULL;
		break;
	case MODE_PIN:
		if (_currentPin != NULL) {
			delete[] _currentPin;
		}
		_currentPin = NULL;
		break;
	case MODE_OPTIONS:
		delete _menuOptions;
		_menuOptions = NULL;
		_activeMenu = NULL;
		break;
	}

	/* Initialize everything that is needed for the new mode. */
	switch(mode) {
	case MODE_KEYBOARD:
		{
			_softwareKeyboard = new SoftwareKeyboard;
			SwitchSubMode(MODE_KEYBOARD_SUB_KEYS);
		}
		break;
	case MODE_PASSWORD_ENTRIES:
		{
			Options* options = OptionsHelper::GetOptions();
			int passwordEntryCount = options->passwordEntryCount;
			delete options;
			_menuPasswordEntries = new Menu(passwordEntryCount, 22, -2, 2, 5, 46, 8); 
			_menuPasswordEntries->SetMenuType(Menu::MENU_TYPE_PASSWORD_ENTRIES);
			_menuMain = new Menu(3, -2, -2, 1, 5, 17, 8); 
			char* tempString;
			tempString = new char[4];
			tempString[0] = 'A'; tempString[1] = 'D'; tempString[2] = 'D'; tempString[3] = '\0';
			_menuMain->AddItem(tempString, COMMAND_ADD_PASSWORD_ENTRY);
			tempString = new char[4];
			tempString[0] = 'O'; tempString[1] = 'P'; tempString[2] = 'T'; tempString[3] = '\0';
			_menuMain->AddItem(tempString, COMMAND_OPTIONS);
			tempString = new char[4];
			tempString[0] = 'E'; tempString[1] = 'X'; tempString[2] = 'P'; tempString[3] = '\0';
			_menuMain->AddItem(tempString, COMMAND_EXPORT);
			SwitchSubMode(MODE_PASSWORD_ENTRIES_SUB_ENTRIES);
		}
		break;
	case MODE_PASSWORD_ENTRY_DETAIL:
		_menuPasswordEntryDetail = new Menu(6, -2, 26, 2, 5, 60, 8); 
		char* tempString;
			tempString = new char[6];
			// Enter
			tempString[0] = 'E'; tempString[1] = 'n'; tempString[2] = 't'; tempString[3] = 'e'; tempString[4] = 'r'; tempString[5] = '\0';
			_menuPasswordEntryDetail->AddItem(tempString, COMMAND_PASSWORD_ENTRY_DETAIL_ENTER);
			tempString = new char[10];
			// Edit name
			tempString[0] = 'E'; tempString[1] = 'd'; tempString[2] = 'i'; tempString[3] = 't'; tempString[4] = ' '; tempString[5] = 'n'; tempString[6] = 'a'; tempString[7] = 'm'; tempString[8] = 'e'; tempString[9] = '\0';
			_menuPasswordEntryDetail->AddItem(tempString, COMMAND_PASSWORD_ENTRY_DETAIL_CHNAME);
			tempString = new char[8];
			// Move up
			tempString[0] = 'M'; tempString[1] = 'o'; tempString[2] = 'v'; tempString[3] = 'e'; tempString[4] = ' '; tempString[5] = 'u'; tempString[6] = 'p'; tempString[7] = '\0';
			_menuPasswordEntryDetail->AddItem(tempString, COMMAND_PASSWORD_ENTRY_DETAIL_MV_UP);
			tempString = new char[8];
			// Edit pw
			tempString[0] = 'E'; tempString[1] = 'd'; tempString[2] = 'i'; tempString[3] = 't'; tempString[4] = ' '; tempString[5] = 'p'; tempString[6] = 'w'; tempString[7] = '\0';
			_menuPasswordEntryDetail->AddItem(tempString, COMMAND_PASSWORD_ENTRY_DETAIL_CHPW);
			tempString = new char[10];
			// Move down
			tempString[0] = 'M'; tempString[1] = 'o'; tempString[2] = 'v'; tempString[3] = 'e'; tempString[4] = ' '; tempString[5] = 'd'; tempString[6] = 'o'; tempString[7] = 'w'; tempString[8] = 'n'; tempString[9] = '\0';
			_menuPasswordEntryDetail->AddItem(tempString, COMMAND_PASSWORD_ENTRY_DETAIL_MV_DOWN);
			tempString = new char[7];
			// Delete
			tempString[0] = 'D'; tempString[1] = 'e'; tempString[2] = 'l'; tempString[3] = 'e'; tempString[4] = 't'; tempString[5] = 'e'; tempString[6] = '\0';
			_menuPasswordEntryDetail->AddItem(tempString, COMMAND_PASSWORD_ENTRY_DETAIL_DEL);
		SwitchActiveMenu(_menuPasswordEntryDetail);
		break;
	case MODE_PIN:
		{
			Options* options = OptionsHelper::GetOptions();
			int pinLength = sizeof(options->pin);
			delete options;
			_currentPin = new char[pinLength];
			StringHelper::Initialize(_currentPin, pinLength);
		}
		break;
	case MODE_OPTIONS:
		{
			_menuOptions = new Menu(1, -2, -2, 1, 5, 124, 8); 
			char* tempString;
			tempString = new char[9];
			// Edit PIN
			tempString[0] = 'E'; tempString[1] = 'd'; tempString[2] = 'i'; tempString[3] = 't'; tempString[4] = ' '; tempString[5] = 'P'; tempString[6] = 'I'; tempString[7] = 'N'; tempString[8] = '\0';
			_menuOptions->AddItem(tempString, COMMAND_EDIT_PIN);
			SwitchActiveMenu(_menuOptions);
		}
		break;
	}
	_mode = mode;
}

void MainController::SwitchSubMode(byte mode) {
	switch(mode) {
	case MODE_KEYBOARD_SUB_KEYS:
		SwitchActiveMenu(_softwareKeyboard->GetMenuKeyboardKeys());
		break;
	case MODE_KEYBOARD_SUB_KEYSETS:
		SwitchActiveMenu(_softwareKeyboard->GetMenuKeyboardKeySets());
		break;
	case MODE_PASSWORD_ENTRIES_SUB_ENTRIES:
		SwitchActiveMenu(_menuPasswordEntries);
		break;
	case MODE_PASSWORD_ENTRIES_SUB_MENU:
		SwitchActiveMenu(_menuMain);
		break;
	}
	_subMode = mode;
}

void MainController::SwitchActiveMenu(Menu* page) {
	if (_activeMenu != NULL) {
		_activeMenu->SetShowPointer(false);
	}
	_activeMenu = page;
	if (_activeMenu != NULL) {
		_activeMenu->SetShowPointer(true);
	}
}

void MainController::EnterPinChar(char newChar) {
	Options* options = OptionsHelper::GetOptions();
	int pinLength = sizeof(options->pin);
	delete options;
	for (int i = 0; i < pinLength - 1; i++) {
		if (_currentPin[i] == '\0') {
			_currentPin[i] = newChar;
			break;
		}
	}
}

void MainController::HandleUserInput() {
	if (_released[0]) { // LEFT-Button
		bool cursorMoved = true;
		if (_activeMenu != NULL) {
			cursorMoved = _activeMenu->CursorLeft();
		}
		switch(_mode) {
		case MODE_KEYBOARD:
			switch(_subMode) {
			case MODE_KEYBOARD_SUB_KEYS:
				if (!cursorMoved) {
					SwitchSubMode(MODE_KEYBOARD_SUB_KEYSETS);
				}
				break;
			}
			break;
		case MODE_PASSWORD_ENTRIES:
			switch(_subMode) {
			case MODE_PASSWORD_ENTRIES_SUB_ENTRIES:
				if (!cursorMoved) {
					SwitchSubMode(MODE_PASSWORD_ENTRIES_SUB_MENU);
				}
				break;
			}
			break;
		case MODE_PIN:
			EnterPinChar('L');
			break;
		}
		return;
	}

	if (_released[1]) { // RIGHT-Button
		bool cursorMoved = true;
		if (_activeMenu != NULL) {
			cursorMoved = _activeMenu->CursorRight();
		}
		switch(_mode) {
		case MODE_KEYBOARD:
			switch(_subMode) {
			case MODE_KEYBOARD_SUB_KEYSETS:
				if (!cursorMoved) {
					SwitchSubMode(MODE_KEYBOARD_SUB_KEYS);
				}
				break;
			}
			break;
		case MODE_PASSWORD_ENTRIES:
			switch(_subMode) {
			case MODE_PASSWORD_ENTRIES_SUB_MENU:
				if (!cursorMoved) {
					SwitchSubMode(MODE_PASSWORD_ENTRIES_SUB_ENTRIES);
				}
				break;
			}
			break;
		case MODE_PIN:
			EnterPinChar('R');
			break;
		}
		return;
	}

	if (_released[2]) { // UP-Button
		if (_activeMenu != NULL) {
			_activeMenu->CursorUp();
		}
		switch(_mode) {
		case MODE_PIN:
			EnterPinChar('U');
			break;
		}
		return;
	}

	if (_released[3]) { // DOWN-Button
		if (_activeMenu != NULL) {
			_activeMenu->CursorDown();
		}
		switch(_mode) {
		case MODE_PIN:
			EnterPinChar('D');
			break;
		}
		return;
	}

	if (_released[5]) { // OK-Button
		MenuItem* selectedItem = NULL;
		if (_activeMenu != NULL) {
			selectedItem = _activeMenu->GetSelectedItem();
		}
		if (selectedItem != NULL) {
			switch(selectedItem->command) {
			/* Commands for keyboard. */
			case COMMAND_KEYBOARD_ADD_CHAR:
				if (selectedItem->displayValue != NULL ) {
					_softwareKeyboard->AddChar(selectedItem->displayValue[0]);
				}
				break;
			case COMMAND_KEYBOARD_KEY_BACKSPACE:
				_softwareKeyboard->DeleteCharBehindPointer();
				break;
			case COMMAND_KEYBOARD_ATOZ_UPPER:
				_softwareKeyboard->SwitchKeyboardSet(SoftwareKeyboard::KEYBOARD_ATOZ_UPPER);
				break;
			case COMMAND_KEYBOARD_ATOZ_LOWER:
				_softwareKeyboard->SwitchKeyboardSet(SoftwareKeyboard::KEYBOARD_ATOZ_LOWER);
				break;
			case COMMAND_KEYBOARD_NUMBERS:
				_softwareKeyboard->SwitchKeyboardSet(SoftwareKeyboard::KEYBOARD_NUMBERS);
				break;
			case COMMAND_KEYBOARD_SPECIALS:
				_softwareKeyboard->SwitchKeyboardSet(SoftwareKeyboard::KEYBOARD_SPECIALS);
				break;
			case COMMAND_KEYBOARD_CONTROLS:
				_softwareKeyboard->SwitchKeyboardSet(SoftwareKeyboard::KEYBOARD_CONTROLS);
				break;
			case COMMAND_KEYBOARD_KEY_LEFT:
				_softwareKeyboard->MovePointer(-1);
				break;
			case COMMAND_KEYBOARD_KEY_RIGHT:
				_softwareKeyboard->MovePointer(1);
				break;
			case COMMAND_KEYBOARD_RND:
				{
					_softwareKeyboard->AddRandomChar();
				}
				break;
			case COMMAND_KEYBOARD_OK:
				switch(_softwareKeyboard->GetOkCommand()) {
				case COMMAND_PASSWORD_ENTRY_DETAIL_CHNAME:
					{
						PasswordEntry* currentPasswordEntry = PasswordEntryController::GetPasswordEntry(_currentPasswordEntryIndex);
						PasswordEntryController::PasswordEntryPut(currentPasswordEntry, _softwareKeyboard->GetCurrentKeyboardString(), String(currentPasswordEntry->password));
						PasswordEntryController::SavePasswordEntry(_currentPasswordEntryIndex, currentPasswordEntry);
						delete currentPasswordEntry;
						PopModeStack();
					}
					break;
				case COMMAND_PASSWORD_ENTRY_DETAIL_CHPW:
					{
						PasswordEntry* currentPasswordEntry = PasswordEntryController::GetPasswordEntry(_currentPasswordEntryIndex);
						PasswordEntryController::PasswordEntryPut(currentPasswordEntry, String(currentPasswordEntry->name), _softwareKeyboard->GetCurrentKeyboardString());
						PasswordEntryController::SavePasswordEntry(_currentPasswordEntryIndex, currentPasswordEntry);
						delete currentPasswordEntry;
						PopModeStack();
					}
					break;
				}
				break;

			/* Special commands. */
			case COMMAND_BY_POINTER_VALUE:
				switch(_subMode) {
				case MODE_PASSWORD_ENTRIES_SUB_ENTRIES:
					int pointer = _menuPasswordEntries->GetPointer();
					_currentPasswordEntryIndex = pointer;
					PushModeStack(MODE_PASSWORD_ENTRY_DETAIL);
					break;
				}
				break;

			/* Commands for entry deail. */
			case COMMAND_PASSWORD_ENTRY_DETAIL_ENTER:
				{
					PasswordEntry* currentPasswordEntry = PasswordEntryController::GetPasswordEntry(_currentPasswordEntryIndex);
					Keyboard.begin();
					KeyboardType(currentPasswordEntry->password);
					Keyboard.end();
					delete currentPasswordEntry;
				}
				break;
			case COMMAND_PASSWORD_ENTRY_DETAIL_CHNAME:
				{
					PushModeStack(MODE_KEYBOARD);
					PasswordEntry* currentPasswordEntry = PasswordEntryController::GetPasswordEntry(_currentPasswordEntryIndex);
					_softwareKeyboard->PrepareKeyboard("Edit name", String(currentPasswordEntry->name), COMMAND_PASSWORD_ENTRY_DETAIL_CHNAME, sizeof(currentPasswordEntry->name)-1);
					delete currentPasswordEntry;
				}
				break;
			case COMMAND_PASSWORD_ENTRY_DETAIL_CHPW:
				{
					PushModeStack(MODE_KEYBOARD);
					PasswordEntry* currentPasswordEntry = PasswordEntryController::GetPasswordEntry(_currentPasswordEntryIndex);
					_softwareKeyboard->PrepareKeyboard("Edit PW", String(currentPasswordEntry->password), COMMAND_PASSWORD_ENTRY_DETAIL_CHPW, sizeof(currentPasswordEntry->password)-1);
					delete currentPasswordEntry;
				}
				break;
			case COMMAND_PASSWORD_ENTRY_DETAIL_DEL:
				PasswordEntryController::DeletePasswordEntry(_currentPasswordEntryIndex);
				PopModeStack();
				break;
			case COMMAND_PASSWORD_ENTRY_DETAIL_MV_UP:
				_currentPasswordEntryIndex = PasswordEntryController::MoveEntry(_currentPasswordEntryIndex, -1);
				break;
			case COMMAND_PASSWORD_ENTRY_DETAIL_MV_DOWN:
				_currentPasswordEntryIndex = PasswordEntryController::MoveEntry(_currentPasswordEntryIndex, 1);
				break;

			/* Commands for main menu. */
			case COMMAND_ADD_PASSWORD_ENTRY:
				{
					PasswordEntry* tempEntry;
					tempEntry = new PasswordEntry;
					Options* options = OptionsHelper::GetOptions();
					int newEntryNumber = options->passwordEntryCount + 1;
					delete options;
					String* tempName = new String("NEW " + String(newEntryNumber, 10));
					PasswordEntryController::PasswordEntryPut(tempEntry, *tempName, "");
					delete tempName;
					if (PasswordEntryController::AddPasswordEntry(tempEntry)) {
						delete tempEntry;
						options = OptionsHelper::GetOptions();
						int passwordEntryCount = options->passwordEntryCount;
						delete options;
						_currentPasswordEntryIndex = passwordEntryCount - 1;
						PushModeStack(MODE_PASSWORD_ENTRY_DETAIL);
					} else {
						delete tempEntry;
					}
				}
				break;
			case COMMAND_OPTIONS:
				PushModeStack(MODE_OPTIONS);
				break;
			case COMMAND_EXPORT:
				{
					Options* options = OptionsHelper::GetOptions();
					int passwordEntryCount = options->passwordEntryCount;
					delete options;
					for (int i = 0; i < passwordEntryCount; i++) {
						PasswordEntry* temp = PasswordEntryController::GetPasswordEntry(i);
						KeyboardType(temp->name);
						Keyboard.begin();
						Keyboard.print("\n");
						Keyboard.end();
						KeyboardType(temp->password);
						Keyboard.begin();
						Keyboard.print("\n");
						Keyboard.end();
						delete temp;
					}
				}
				break;

			/* Commands for options menu. */
			case COMMAND_EDIT_PIN:
				_pinCommand = COMMAND_PIN_EDIT;
				PushModeStack(MODE_PIN);
				return;
				break;
			
			default:
				
				break;
			}
		}

		switch(_mode) {
		case MODE_PIN:
			switch(_pinCommand) {
			case COMMAND_PIN_START:
				{
					Options* options = OptionsHelper::GetOptions();

					if (StringHelper::Equals(_currentPin, options->pin)) {
						delete options;
						PushModeStack(MODE_PASSWORD_ENTRIES);
					} else {
						delete options;
						StringHelper::Initialize(_currentPin, sizeof(options->pin));
					}
				}
				break;
			case COMMAND_PIN_EDIT:
				{
					_pinCommand = COMMAND_PIN_START;
					Options* options = OptionsHelper::GetOptions();
					StringHelper::Fill(options->pin, sizeof(options->pin), _currentPin);
					OptionsHelper::SaveOptions(options);
					delete options;
					PopModeStack();
				}
				break;
			}
			break;
		}
		return;
	}

	if (_released[4]) { // BACK-Button
		PopModeStack();
		return;
	}
}

void MainController::KeyboardType(char* str) {
	Keyboard.begin();
	int i = 0;
	while(true) {
		if (str[i] == '\0') {
			break;
		}
		char* temp = StringHelper::GetCharArrayWithStop(pgm_read_byte(&CHARS_US_TO_DE[str[i]]));
		Keyboard.print(temp);
		delete[] temp;
		i++;
	}
	Keyboard.end();
}
