#include "OptionsHelper.h"

bool OptionsHelper::ValidateOptions(Options* options) {
	if (options == NULL) {
		return false;
	}
	if (options->version[sizeof(options->version) - 1] != 0) {
		return false;
	}
	String savedVersion = String(options->version);
	if (!savedVersion.equals("1.0")) {
		return false;
	}
	return true;
}

Options* OptionsHelper::GetOptions() {
	Options* options = new Options;
	EEPROM.get(EEPROM_ADDRESS_OPTIONS, *options);
	if (!ValidateOptions(options)) {
		InitializeOptions(options);
		SaveOptions(options);
	}
	return options;
}

void OptionsHelper::SaveOptions(Options* options) {
	EEPROM.put(EEPROM_ADDRESS_OPTIONS, *options);
}

void OptionsHelper::InitializeOptions(Options* options) {
	options->passwordEntryCount = 0;
	
	String pin = "";
	StringHelper::Fill(options->pin, sizeof(options->pin), pin);

	String version = "1.0";
	StringHelper::Fill(options->version, sizeof(options->version), version);
}

