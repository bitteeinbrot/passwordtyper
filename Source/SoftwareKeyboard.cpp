#include "SoftwareKeyboard.h"

SoftwareKeyboard::SoftwareKeyboard() {
	_currentKeyboardString = new char[STRING_SIZE_KEYBOARD_VALUE];
	StringHelper::Initialize(_currentKeyboardString, STRING_SIZE_KEYBOARD_VALUE);
	_keyboardCaption = new char[STRING_SIZE_KEYBOARD_CAPTION];
	StringHelper::Initialize(_keyboardCaption, STRING_SIZE_KEYBOARD_CAPTION);
	_keyboardPointer = 0;
	_keyboardOkCommand = MODE_NONE;
	_keyboardMaxChars = 0;

	char* tempString;
	_menuKeyboardKeys = CreateKeyboard(KEYBOARD_ATOZ_LOWER);
	_menuKeyboardKeySets = new Menu(6, -2, 26, 2, 3, 17, 8);
	tempString = new char[4];
	tempString[0] = 'a'; tempString[1] = 'b'; tempString[2] = 'c'; tempString[3] = '\0';
	_menuKeyboardKeySets->AddItem(tempString, COMMAND_KEYBOARD_ATOZ_LOWER);
	tempString = new char[4];
	tempString[0] = 'A'; tempString[1] = 'B'; tempString[2] = 'C'; tempString[3] = '\0';
	_menuKeyboardKeySets->AddItem(tempString, COMMAND_KEYBOARD_ATOZ_UPPER);
	tempString = new char[4];
	tempString[0] = '1'; tempString[1] = '2'; tempString[2] = '3'; tempString[3] = '\0';
	_menuKeyboardKeySets->AddItem(tempString, COMMAND_KEYBOARD_NUMBERS);
	tempString = new char[4];
	tempString[0] = '!'; tempString[1] = '?'; tempString[2] = '+'; tempString[3] = '\0';
	_menuKeyboardKeySets->AddItem(tempString, COMMAND_KEYBOARD_SPECIALS);
	tempString = new char[4];
	tempString[0] = 'C'; tempString[1] = 'N'; tempString[2] = 'T'; tempString[3] = '\0';
	_menuKeyboardKeySets->AddItem(tempString, COMMAND_KEYBOARD_CONTROLS);
	tempString = new char[3];
	tempString[0] = 'O'; tempString[1] = 'K'; tempString[2] = '\0';
	_menuKeyboardKeySets->AddItem(tempString, COMMAND_KEYBOARD_OK);	
}

SoftwareKeyboard::~SoftwareKeyboard() {
	if (_menuKeyboardKeys != NULL) {
		delete _menuKeyboardKeys;
		_menuKeyboardKeys = NULL;
	}
	if (_menuKeyboardKeySets != NULL) {
		delete _menuKeyboardKeySets;
		_menuKeyboardKeySets = NULL;
	}
	delete[] _currentKeyboardString;
	delete[] _keyboardCaption;
}

void SoftwareKeyboard::PrepareKeyboard(String caption, String value, int okCommand, int keyboardMaxChars) {
	StringHelper::Fill(_keyboardCaption, STRING_SIZE_KEYBOARD_CAPTION, caption);
	StringHelper::Fill(_currentKeyboardString, STRING_SIZE_KEYBOARD_VALUE, value);
	_keyboardPointer = StringHelper::Length(_currentKeyboardString, STRING_SIZE_KEYBOARD_VALUE);
	_keyboardOkCommand = okCommand;
	_keyboardMaxChars = keyboardMaxChars;

}

Menu* SoftwareKeyboard::CreateKeyboard(byte keyboard) {
	int slotHeight = 8;
	int slotWidth = 5;
	int posX = 49;
	int posY = 26;
	int colums = 8;
	int rows = 3;

	Menu* tempKeyboard = NULL;
	switch(keyboard) {
	case KEYBOARD_ATOZ_LOWER:
		tempKeyboard = new Menu((122-97+1), posX, posY, colums, rows, slotWidth, slotHeight);
		for(int i = 97; i <= 122; i++) {
			tempKeyboard->AddItem(StringHelper::GetCharArrayWithStop((char) i), COMMAND_KEYBOARD_ADD_CHAR);
		}
		break;
	case KEYBOARD_ATOZ_UPPER:
		tempKeyboard = new Menu((90-65+1), posX, posY, colums, rows, slotWidth, slotHeight);
		for(int i = 65; i <= 90; i++) {
			tempKeyboard->AddItem(StringHelper::GetCharArrayWithStop((char) i), COMMAND_KEYBOARD_ADD_CHAR);
		}
		break;
	case KEYBOARD_NUMBERS:
		tempKeyboard = new Menu((57-48+1), posX, posY, colums, rows, slotWidth, slotHeight);
		for(int i = 48; i <= 57; i++) {
			tempKeyboard->AddItem(StringHelper::GetCharArrayWithStop((char) i), COMMAND_KEYBOARD_ADD_CHAR);
		}
		break;
	case KEYBOARD_SPECIALS:
		tempKeyboard = new Menu(SPECIAL_CHARS_SIZE, posX, posY, colums, rows, slotWidth, slotHeight);
		tempKeyboard->SetMenuType(Menu::MENU_TYPE_KEYBOARD_SPECIALS);
		break;
	case KEYBOARD_CONTROLS:
		tempKeyboard = new Menu(4, posX, posY, 3, rows, 17, slotHeight);
		tempKeyboard->AddItem(NULL, COMMAND_KEYBOARD_KEY_LEFT);
		tempKeyboard->AddItem(NULL, COMMAND_KEYBOARD_KEY_RIGHT);
		tempKeyboard->AddItem(StringHelper::GetCharArrayFromString("DEL"), COMMAND_KEYBOARD_KEY_BACKSPACE);
		tempKeyboard->AddItem(StringHelper::GetCharArrayFromString("RND"), COMMAND_KEYBOARD_RND);
		break;
	}
	return tempKeyboard;
}

void SoftwareKeyboard::Render(Adafruit_SSD1306* display) {
	/* Render caption. */
	display->setCursor(0, 0);
	display->print(_keyboardCaption);

	/* Render entered string. */
	display->setCursor(0, 12);
	display->print(_currentKeyboardString);

	/* Showng remaining chars. */
	String remainingChars = "(" + String(StringHelper::Length(_currentKeyboardString, STRING_SIZE_KEYBOARD_VALUE), 10) + "/" + String((int) _keyboardMaxChars, 10) + ")";
	display->setCursor(127 - remainingChars.length()*6, 0);
	display->print(remainingChars);

	/* Render keyboard pointer. */
	_menuKeyboardKeys->Render(display);
	_menuKeyboardKeySets->Render(display);
	display->drawLine(0 + _keyboardPointer*6, 20, 6 + _keyboardPointer*6, 20, WHITE);

	/* Some fancy lines. */
	display->drawLine(46, 25, 46, 64, WHITE);
	display->drawLine(0, 25, 128, 25, WHITE);
}

void SoftwareKeyboard::SwitchKeyboardSet(byte setEnum) {
	delete _menuKeyboardKeys;
	_menuKeyboardKeys = CreateKeyboard(setEnum);
}

void SoftwareKeyboard::MovePointer(int direction) {
	if (direction < 0) {
		if (_keyboardPointer > 0) {
			_keyboardPointer--;
		}
	} else if (direction > 0) {
		if (_keyboardPointer < StringHelper::Length(_currentKeyboardString, STRING_SIZE_KEYBOARD_VALUE)) {
			_keyboardPointer++;
		}
	}
}

void SoftwareKeyboard::DeleteCharBehindPointer() {
	if (StringHelper::Length(_currentKeyboardString, STRING_SIZE_KEYBOARD_VALUE) > 0) {
		if (_keyboardPointer > 0) {
			StringHelper::DeleteChar(_keyboardPointer - 1, _currentKeyboardString, STRING_SIZE_KEYBOARD_VALUE);
				if (_keyboardPointer > 0) {
				_keyboardPointer--;
			}
		}
	}
}

void SoftwareKeyboard::AddRandomChar() {
	int max = 62 + SPECIAL_CHARS_SIZE; //(122-97+1) + (90-65+1) + (57-48+1) + SPECIAL_CHARS_SIZE;
	randomSeed(millis());
	int randomInt = random(max);
	int iterator = 0;
	char randomChar = '?';


	byte* allChars = new byte[3*2];
	/* a-z */
	allChars[0] = 97;
	allChars[1] = 122;
	/* A-Z */
	allChars[2] = 65;
	allChars[3] = 90;
	/* 0-9 */
	allChars[4] = 48;
	allChars[5] = 57;
			
	for (byte n = 0; n < 3*2; n = n + 2) {
		for(int i = allChars[n]; i <= allChars[n+1]; i++) {
			if (iterator == randomInt) {
				randomChar = (char) i;
			}
			iterator++;
		}
	}

	delete[] allChars;

	for (int i = 0; i < SPECIAL_CHARS_SIZE; i++) {
		if (iterator == randomInt) {
			randomChar = pgm_read_byte(&SPECIAL_CHARS[i]);
		}
		iterator++;
	}

	if (_keyboardMaxChars > StringHelper::Length(_currentKeyboardString, STRING_SIZE_KEYBOARD_VALUE)) {
		StringHelper::InsertChar(_keyboardPointer, _currentKeyboardString, STRING_SIZE_KEYBOARD_VALUE, randomChar);
		_keyboardPointer++;
	}
}

void SoftwareKeyboard::AddChar(char charToAdd) {
	if (_keyboardMaxChars > StringHelper::Length(_currentKeyboardString, STRING_SIZE_KEYBOARD_VALUE)) {
		StringHelper::InsertChar(_keyboardPointer, _currentKeyboardString, STRING_SIZE_KEYBOARD_VALUE, charToAdd);
		_keyboardPointer++;
	}
}

Menu* SoftwareKeyboard::GetMenuKeyboardKeys() {
	return _menuKeyboardKeys;
}

Menu* SoftwareKeyboard::GetMenuKeyboardKeySets() {
	return _menuKeyboardKeySets;
}

char* SoftwareKeyboard::GetCurrentKeyboardString() {
	return _currentKeyboardString;
}

byte SoftwareKeyboard::GetOkCommand() {
	return _keyboardOkCommand;
}
