#include "Menu.h"

Menu::Menu(uint8_t itemCount, int8_t posX, int8_t posY, uint8_t colums, uint8_t rows, uint8_t slotWidth, uint8_t slotHeight) {
	_posX = posX;
	_posY = posY;
	_colums = colums;
	_pointer = 0;
	_items = new MenuItem*[itemCount];
	for (uint8_t i = 0; i < itemCount; i++) {
		_items[i] = NULL;
	}
	_itemArrayPointer = 0;
	_itemCount = itemCount;
	_slotWidth = slotWidth;
	_slotHeight = slotHeight;
	_rows = rows;
	_showPointer = false;
	_scrollBar = true;
	_menuType = PAGE_TYPE_STANDARD;
}

Menu::~Menu() {
	DeleteItems();
	delete[] _items;
}

void Menu::DeleteItems() {
	for (uint8_t i = 0; i < _itemCount; i++) {
		if (_items[i] != NULL) {
			delete[] _items[i]->displayValue;
			delete _items[i];
			_items[i] = NULL;
		}
	}
}

void Menu::AddItem(char* displayValue, byte command) {
	MenuItem* tempItem = new MenuItem;
	tempItem->command = command;
	//char *cstr = new char[displayValue.length() + 1]; // +1 wegen NULL Byte am Ende.
	//strcpy(cstr, displayValue.c_str());
	tempItem->displayValue = displayValue;
	_items[_itemArrayPointer] = tempItem;
	_itemArrayPointer++;
}

void Menu::Render(Adafruit_SSD1306* display) {
	int8_t maxRows = _itemCount/_colums;
	if (_itemCount%_colums > 0) {
		maxRows++;
	}

	int8_t maxItemIndex;
	int8_t indexOffset;
	int8_t startRowIndex;
	{
		/* Get start row. */
		int8_t pointerRowIndex = _pointer/_colums;
		startRowIndex = 0;
		if (maxRows > _rows) {
			if (_rows%2 > 0) {
				startRowIndex = pointerRowIndex - _rows/2;
			} else {
				startRowIndex = pointerRowIndex - _rows/2 + 1;
			}
		}
	
	
		int8_t shiftEndRow = 0;
		if (startRowIndex < 0) {
			shiftEndRow += startRowIndex*-1;
			startRowIndex = 0;
		}
		/* Get end row. */
		int8_t endRowIndex = _rows - 1;
		if (maxRows > _rows) {
			endRowIndex = pointerRowIndex + _rows/2 + shiftEndRow;
		}
	
		int8_t shiftStartRow = 0;
		if (endRowIndex > maxRows - 1) {
			shiftStartRow = maxRows - 1 - endRowIndex;
			endRowIndex = maxRows - 1;
		}

		/* Shift start row. */
		if (startRowIndex + shiftStartRow >= 0) {
			startRowIndex = startRowIndex + shiftStartRow;
		}

		/* Calc min and max item index. */
		indexOffset = startRowIndex*_colums;
		maxItemIndex = (endRowIndex+1)*_colums;
		if (maxItemIndex > _itemCount) {
			maxItemIndex = _itemCount;
		}
		maxItemIndex = maxItemIndex - indexOffset;
	}

	for (int8_t i = 0; i < maxItemIndex; i++)  {
		int8_t offX = (i%_colums) * (_slotWidth + MARGIN + MARGIN_OUTER);
		int8_t offY = i / _colums * (_slotHeight + MARGIN + MARGIN_OUTER);
		RenderItem(GetItem(i + indexOffset), _posX + offX + MARGIN + MARGIN_OUTER, _posY + offY + MARGIN + MARGIN_OUTER, display);
		if (_showPointer && i + indexOffset == _pointer) {
			display->drawRect(_posX + offX + MARGIN_OUTER, _posY + offY + MARGIN_OUTER, _slotWidth + MARGIN*2, _slotHeight + MARGIN*2, WHITE);
		}
	}

	if (_scrollBar && maxRows > _rows) {
		int8_t offsetX = (_slotWidth + MARGIN + MARGIN_OUTER)*_colums;
		int8_t height = (_slotHeight + MARGIN + MARGIN_OUTER)*_rows;
		display->drawRect(_posX + offsetX + 3, _posY + MARGIN_OUTER, 3, height, WHITE);
		int8_t innerHeight = height-2;
		int8_t barHeight = (int8_t)((float)innerHeight/((float)maxRows/(float)_rows));
		int8_t pages = maxRows-_rows/2;
		int8_t barTickSize = innerHeight;
		if (pages > 0) {
			barTickSize = (int8_t)((float)innerHeight/(float)pages);
		}
		int8_t upperBarPos = _posY + MARGIN_OUTER + 1 + startRowIndex*barTickSize;
		display->drawRect(_posX + offsetX + 4, upperBarPos, 1, barHeight, WHITE);
	}
}

void Menu::RenderItem(MenuItem* item, int8_t offsetX, int8_t offsetY, Adafruit_SSD1306* display) {
	if (item == NULL) {
		display->setCursor(offsetX, offsetY);
		display->print('?');
		return;
	}
	if (item->displayValue != NULL) {
		/* Renders the string and mekes sure it is not bigger than the slot size.  */
		display->setCursor(offsetX, offsetY);
		int maxPossibleLength = (_slotWidth+2) / 6;
		char* tempString = new char[maxPossibleLength + 1];
		StringHelper::Initialize(tempString, maxPossibleLength + 1);
		for (int i = 0; i < maxPossibleLength; i++) {
			tempString[i] = item->displayValue[i];
		}
		tempString[maxPossibleLength] = '\0';
		display->print(tempString);
		delete[] tempString;
		return;
	}
	switch(item->command) {
	case COMMAND_KEYBOARD_KEY_LEFT:
		display->drawLine(offsetX + 1, offsetY + 3, offsetX + 15, offsetY + 3, WHITE);
		display->drawLine(offsetX + 2, offsetY + 2, offsetX + 2, offsetY + 4, WHITE);
		display->drawLine(offsetX + 3, offsetY + 1, offsetX + 3, offsetY + 5, WHITE);
		break;
	case COMMAND_KEYBOARD_KEY_RIGHT:
		display->drawLine(offsetX + 1, offsetY + 3, offsetX + 15, offsetY + 3, WHITE);
		display->drawLine(offsetX + 14, offsetY + 2, offsetX + 14, offsetY + 4, WHITE);
		display->drawLine(offsetX + 13, offsetY + 1, offsetX + 13, offsetY + 5, WHITE);
		break;
	default:
		display->setCursor(offsetX, offsetY);
		display->print('?');
		break;
	}
}

bool Menu::CursorRight() {
	if (_menuType != PAGE_TYPE_STANDARD) {
		DeleteItems();
	}
	if ((_pointer+1)%_colums > 0 && _pointer < _itemCount - 1) {
		_pointer++;
		return true;
	}
	return false;
}

bool Menu::CursorLeft() {
	if (_menuType != PAGE_TYPE_STANDARD) {
		DeleteItems();
	}
	if ((_pointer+1)%_colums != 1 && _pointer > 0) {
		_pointer--;
		return true;
	}
	return false;
}

bool Menu::CursorUp() {
	if (_menuType != PAGE_TYPE_STANDARD) {
		DeleteItems();
	}
	if (_pointer - _colums >= 0) {
		_pointer -= _colums;
		return true;
	}
	return false;
}

bool Menu::CursorDown() {
	if (_menuType != PAGE_TYPE_STANDARD) {
		DeleteItems();
	}
	if (_pointer + _colums < _itemCount) {
		_pointer += _colums;
		return true;
	}
	
	int maxRows = _itemCount/_colums;
	if (_itemCount%_colums > 0) {
		maxRows++;
	}
	int pointerRowIndex = _pointer/_colums;

	if (_pointer < _itemCount - 1 && pointerRowIndex < maxRows-1) {
		_pointer = _itemCount - 1;
		return true;
	}
	return false;
}

void Menu::SetPointer(uint8_t pointer) {
	if (_menuType != PAGE_TYPE_STANDARD) {
		DeleteItems();
	}
	if (pointer >= 0 && pointer < _itemCount) {
		_pointer = pointer;
	}
}

int Menu::GetPointer() {
	return _pointer;
}

MenuItem* Menu::GetSelectedItem() {
	return _items[_pointer];
}

MenuItem* Menu::GetItem(uint8_t index) {
	MenuItem* item = _items[index];
	if (item != NULL) {
		return item;
	}
	_items[index] = MainController::GetInstance()->GetMenuItem(index, _menuType);
	return _items[index];
}

void Menu::SetMenuType(uint8_t type) {
	_menuType = type;
}

void Menu::SetShowPointer(bool state) {
	_showPointer = state;
}

void Menu::SetScrollbar(bool enabled) {
	_scrollBar = enabled;
}
