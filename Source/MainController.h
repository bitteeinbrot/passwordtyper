#ifndef _MAINCONTROLLER_h
#define _MAINCONTROLLER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <EEPROM.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Wire.h>
#include <Keyboard.h>
#include <avr/pgmspace.h>

#include "Structs.h"
#include "StringHelper.h"

class Menu;
class OptionsHelper;
class SoftwareKeyboard;

const byte MAX_PASSWORD_ENTRIES = 30; // Maximum amount of password entries. Is limited by EEPROM size.

const byte EEPROM_ADDRESS_OPTIONS = 0;
const byte EEPROM_ADDRESS_PASSWORD_ENTRIES = 40;

/* ------------------------------- START ---------------------------------- */
/* Lookup table to transfer ASCII from US to DE. */
const char CHARS_US_TO_DE[123] PROGMEM = {
//  0,  0,  0,  0,  0,  0,  0,  0, BS, TB, CR,  0,  0,  0,  0,  0,
	0,  0,  0,  0,  0,  0,  0,  0,  8,  9, 10,  0,  0, 13,  0,  0,
	0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
//  BL,  !,  �,  �,  $,  %,  /,  �,  ),  =,  (,  `,  ,,  �,  .,  -,
//           "               &   /   (   )   *   +       -       /
	32, 33, 64,  0, 36, 37, 94, 38, 42, 40,125,184, 44, 47, 46, 38,
//   0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  �,  �,  ;,  �,  :,  _,
//                                           :   ;       =       ?
	48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 62, 60,  0, 41,  0, 95,
//  ",  A,  B,  C,  D,  E,  F,  G,  H,  I,  J,  K,  L,  M,  N,  O,
//  0
	0, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79,
//   P,  Q,  R,  S,  T,  U,  V,  W,  X,  Z,  Y,  �,  #,  +,  &,  ?,
//                                       Y   Z   [   \   ]   ^   _
	80, 81, 82, 83, 84, 85, 86, 87, 88, 90, 89,  0,  0,  0, 96, 63,
//  ^,  a,  b,  c,  d,  e,  f,  g,  h,  i,  j,  k,  l,  m,  n,  o,
//  `
	43, 97, 98, 99,100,101,102,103,104,105,106,107,108,109,110,111,
//    p,  q,  r,  s,  t,  u,  v,  w,  x,  z,  y  
//                                        y   z   
	112,113,114,115,116,117,118,119,120,122,121
};
/* -------------------------------- END ----------------------------------- */

/* ------------------------------- START ---------------------------------- */
/* Special chars. */
const byte SPECIAL_CHARS_SIZE = 17;
/* All available special chars. */
const char SPECIAL_CHARS[SPECIAL_CHARS_SIZE] PROGMEM = {
//    ,  !,  ",  $,  %,  &,  /,  (,  ),  *,  +,  -,  :,  ;,  =,  ?,  _,
	32, 33, 34, 36, 37, 38, 47, 40, 41, 42, 43, 45, 58, 59, 61, 63, 95,
};
/* -------------------------------- END ----------------------------------- */

/* ------------------------------- START ---------------------------------- */
/* All button pins. */
const byte BUTTON_LEFT = 14;
const byte BUTTON_RIGTH = 16;
const byte BUTTON_TOP = 10;
const byte BUTTON_BOTTOM = 7;
const byte BUTTON_BACK = 9;
const byte BUTTON_OK = 8;
/* -------------------------------- END ----------------------------------- */

/* ------------------------------- START ---------------------------------- */
/* All mode IDs. */
const byte MODE_NONE = 0;
const byte MODE_KEYBOARD = 1;
const byte MODE_KEYBOARD_SUB_KEYS = 1;
const byte MODE_KEYBOARD_SUB_KEYSETS = 2;
const byte MODE_PASSWORD_ENTRIES = 3;
const byte MODE_PASSWORD_ENTRIES_SUB_ENTRIES = 4;
const byte MODE_PASSWORD_ENTRIES_SUB_MENU = 5;
const byte MODE_PASSWORD_ENTRY_DETAIL = 6;
const byte MODE_PIN = 7;
const byte MODE_OPTIONS = 8;
/* -------------------------------- END ----------------------------------- */

/* ------------------------------- START ---------------------------------- */
/* All command IDs. */
const byte COMMAND_NONE = 0;
const byte COMMAND_KEYBOARD_ATOZ_LOWER = 1;
const byte COMMAND_KEYBOARD_ATOZ_UPPER = 2;
const byte COMMAND_KEYBOARD_NUMBERS = 4;
const byte COMMAND_KEYBOARD_SPECIALS = 5;
const byte COMMAND_KEYBOARD_CONTROLS = 8;
const byte COMMAND_KEYBOARD_KEY_LEFT = 6;
const byte COMMAND_KEYBOARD_KEY_RIGHT = 7;
const byte COMMAND_KEYBOARD_KEY_BACKSPACE = 9;
const byte COMMAND_KEYBOARD_OK = 10;
const byte COMMAND_KEYBOARD_RND = 23; 
const byte COMMAND_KEYBOARD_ADD_CHAR = 27; //  <---------- Highest number

const byte COMMAND_BY_POINTER_VALUE = 11;
const byte COMMAND_PASSWORD_ENTRY_DETAIL_ENTER = 12;
const byte COMMAND_PASSWORD_ENTRY_DETAIL_CHNAME = 13;
const byte COMMAND_PASSWORD_ENTRY_DETAIL_CHPW = 14;
const byte COMMAND_PASSWORD_ENTRY_DETAIL_DEL = 15;
const byte COMMAND_PASSWORD_ENTRY_DETAIL_MV_UP = 24;
const byte COMMAND_PASSWORD_ENTRY_DETAIL_MV_DOWN = 25; 

const byte COMMAND_OPTIONS = 16;
const byte COMMAND_ADD_PASSWORD_ENTRY = 17;
const byte COMMAND_EXPORT = 26; 

const byte COMMAND_PIN_START = 18;
const byte COMMAND_PIN_EDIT = 21; 

const byte COMMAND_EDIT_PIN = 19;
/* -------------------------------- END ----------------------------------- */

class MainController
{
	private:
		static MainController* _instance;
		
		Adafruit_SSD1306* _display;

		/* ------------------------------- START ---------------------------------- */
		/* --- Hardware keyboard */
		byte* _buttons; // Contains the pin numbers the buttons are connected to.
		bool* _states; // Contains the current states of the buttons.
		bool* _released; // Shows if any button was released within the current loop.

		void HandleUserInput();
		/* -------------------------------- END ----------------------------------- */

		/* ------------------------------- START ---------------------------------- */
		/* --- Everything to handle modes. Modes are more or less the different screens that will be displayed. */
		byte _mode; // ID of the current mode.
		byte _subMode; // ID of the current sub mode.
		byte* _modeStack; // An array where history of modes will be saved.
		byte _modeStackPointer; // Points to next free position in the _modeStack array.
		Menu* _activeMenu; // The currently active menu.

		void PopModeStack();
		void PushModeStack(int8_t mode);
		void SwitchMode(byte mode);
		void SwitchSubMode(byte mode);
		void SwitchActiveMenu(Menu* page);
		/* -------------------------------- END ----------------------------------- */

		/* ------------------------------- START ---------------------------------- */
		/* --- Everything for mode: MODE_KEYBOARD */
		SoftwareKeyboard* _softwareKeyboard;
		/* -------------------------------- END ----------------------------------- */

		/* ------------------------------- START ---------------------------------- */
		/* --- Everything for mode: MODE_PASSWORD_ENTRIES */
		Menu* _menuPasswordEntries; // Contains the menu with all password entries, if the mode MODE_PASSWORD_ENTRIES is active.
		Menu* _menuMain; // Contains the main menu, if the mode MODE_PASSWORD_ENTRIES is active.
		/* -------------------------------- END ----------------------------------- */
		
		/* ------------------------------- START ---------------------------------- */
		/* --- Everything for mode: MODE_PASSWORD_ENTRY_DETAIL */
		int _currentPasswordEntryIndex; // Holds the index of the selected password entry.
		Menu* _menuPasswordEntryDetail; // Contains the menu to control the selected password entry, if the mode MODE_PASSWORD_ENTRY_DETAIL is active.
		/* -------------------------------- END ----------------------------------- */

		/* ------------------------------- START ---------------------------------- */
		/* --- Everything for mode: MODE_PIN */
		char* _currentPin; // Holds the currently entered PIN.
		int _pinCommand; // Command executed after entering PIN.
		void EnterPinChar(char newChar);
		/* -------------------------------- END ----------------------------------- */

		/* ------------------------------- START ---------------------------------- */
		/* --- Everything for mode: MODE_OPTIONS */
		Menu* _menuOptions; // Contains the options menu, if the mode MODE_OPTIONS is active.
		/* -------------------------------- END ----------------------------------- */

		MainController();

		/// <summary>
		/// Renders everything for the current mode.
		/// </summary>
		void Render();
		
		/// <summary>
		/// Types the given string and respects the defined keyboard layout.
		/// </summary>
		/// <param name="str">String that should get typed.</param>
		void KeyboardType(char* str);
	public:
		static MainController* GetInstance();
		
		void Setup(Adafruit_SSD1306* display);
		void Loop();
		
		/// <summary>
		/// Provides a menu item to specific menus. This method was implemented to save RAM. Menu items for big menus should only be loaded to RAM on demand.
		/// </summary>
		/// <param name="index">Index of the menu item.</param>
		/// <param name="menuType">Type of the menu.</param>
		MenuItem* GetMenuItem(int8_t index, int8_t menuType);
};

#endif

