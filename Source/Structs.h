#ifndef _STRUCTS_h
#define _STRUCTS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

typedef struct {
	char name[11];
	char password[21]; 
} PasswordEntry;

typedef struct {
	char version[4];
	char pin[7];
	int passwordEntryCount;
} Options;

typedef struct {
     const char* displayValue;
     uint8_t command;
} MenuItem;

#endif