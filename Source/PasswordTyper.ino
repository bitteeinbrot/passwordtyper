#include "MainController.h"

#define SDA_PIN 2
#define SCL_PIN 3

#define OLED_RESET -1



void setup() {
	delay(2000); // Sometimes the display does not show anything when the microcontroller is plugged in. This delay seems to solve the problem.
	Adafruit_SSD1306* _display = new Adafruit_SSD1306(OLED_RESET);
	_display->begin(SSD1306_SWITCHCAPVCC, 0x3C);
	MainController::GetInstance()->Setup(_display);
}

void loop() {
	MainController::GetInstance()->Loop();
}

