#ifndef _OPTIONSHELPER_h
#define _OPTIONSHELPER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "MainController.h"
#include "StringHelper.h"

class OptionsHelper {
	protected:
		/// <summary>
		/// Initializes the given options sruct. PIN will be set to "".
		/// </summary>
		/// <param name="options">Options to be initialized.</param>
		static void InitializeOptions(Options* options);
		
		/// <summary>
		/// Validates the given options. This function checks, if options were saved to the EEPROM.
		/// </summary>
		/// <param name="options">Options to be validated.</param>
		static bool ValidateOptions(Options* options);

	public:
		/// <summary>
		/// Gets the options from the EEPROM. If no options are stored in EEPROM, a new options struct gets initialized.
		/// </summary>
		static Options* GetOptions();
		
		/// <summary>
		/// Saves the given options to the EEPROM.
		/// </summary>
		/// <param name="options">Options to save.</param>
		static void SaveOptions(Options* options);
};

#endif

