#ifndef _PASSWORDENTRYCONTROLLER_h
#define _PASSWORDENTRYCONTROLLER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "MainController.h"
#include "Structs.h"
#include "OptionsHelper.h"
#include "StringHelper.h"

class PasswordEntryController {
	public:
		static void PasswordEntryPut(PasswordEntry* pwEntry, String name, String password);
		static bool AddPasswordEntry(PasswordEntry* entry);
		static void SavePasswordEntry(int index, PasswordEntry* entry);
		static void DeletePasswordEntry(int index);
		static PasswordEntry* GetPasswordEntry(int index);
		static byte MoveEntry(byte index, int direction);
};

#endif

