#ifndef _SOFTWAREKEYBOARD_h
#define _SOFTWAREKEYBOARD_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <Adafruit_SSD1306.h>
#include "Structs.h"
#include "StringHelper.h"
#include "Menu.h"

class SoftwareKeyboard {
	protected:
		Menu* _menuKeyboardKeys;
		Menu* _menuKeyboardKeySets;
		char* _currentKeyboardString;
		char* _keyboardCaption;
		int _keyboardPointer;
		byte _keyboardOkCommand;
		int _keyboardMaxChars;

		
	public:
		static const byte KEYBOARD_ATOZ_LOWER = 0;
		static const byte KEYBOARD_ATOZ_UPPER = 1;
		static const byte KEYBOARD_NUMBERS = 2;
		static const byte KEYBOARD_SPECIALS = 3;
		static const byte KEYBOARD_CONTROLS = 4;

		static const byte STRING_SIZE_KEYBOARD_VALUE = 21;
		static const byte STRING_SIZE_KEYBOARD_CAPTION = 11;

		SoftwareKeyboard();
		~SoftwareKeyboard();
		Menu* CreateKeyboard(byte keyboard);
		void PrepareKeyboard(String caption, String value, int okCommand, int keyboardMaxChars);
		Menu* GetMenuKeyboardKeys();
		Menu* GetMenuKeyboardKeySets();
		void Render(Adafruit_SSD1306* display);
		void SwitchKeyboardSet(byte setEnum);
		void MovePointer(int direction);
		void DeleteCharBehindPointer();
		void AddRandomChar();
		char* GetCurrentKeyboardString();
		byte GetOkCommand();
		void AddChar(char charToAdd);

};

#endif

