#ifndef _MENU_h
#define _MENU_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <Adafruit_SSD1306.h>
#include "Structs.h"
#include "MainController.h"
#include "StringHelper.h"

class Menu {
	protected:
		static const uint8_t MARGIN = 2;
		static const uint8_t MARGIN_OUTER = 2;

		int8_t _posX; // X-position on display.
		int8_t _posY; // Y-position on display.
		uint8_t _colums; // Number of colums to be displayed.
		uint8_t _rows; // Number of rows to be displayed.
		uint8_t _pointer; // Contains the index of the currently selected menu item.
		MenuItem** _items; // All menu items. Can contain NULLs that have to be replaced in the GetItem-function.
		uint8_t _itemArrayPointer; // Used for the AddItem functions to put the given menu item to the right spot.
		uint8_t _itemCount; // Size of the _items array.
		uint8_t _slotWidth; // Width of the item slot in pixel.
		uint8_t _slotHeight; // Height of the item slot in pixel.
		bool _showPointer; // Indicates if the currently selected item gets highlighted.
		bool _scrollBar; // Indecates if the scroll bar should be rendered.
		uint8_t _menuType; // Type of the menu. Gets used by the MainController to give the correct menu items, if asked.

		void RenderItem(MenuItem* item, int8_t offsetX, int8_t offsetY, Adafruit_SSD1306* display);

		/// <summary>
		/// Acquires the menu item for the given index. At first the item gets searched in _items. If its not there yet, the funxtion will askt the MainController for the item.
		/// </summary>
		/// <param name="index">Index of the menu item.</param>
		MenuItem* GetItem(uint8_t index);

		/// <summary>
		/// Deletes every item in _items.
		/// </summary>
		void DeleteItems();
	
	public:
		static const uint8_t PAGE_TYPE_STANDARD = 0;
		static const uint8_t MENU_TYPE_PASSWORD_ENTRIES = 1;
		static const uint8_t MENU_TYPE_KEYBOARD_SPECIALS = 2;

		/// <param name="itemCount">The amount of menu items the menu will hold.</param>
		/// <param name="posX">The x-position in pixels on the display.</param>
		/// <param name="posY">The y-position in pixels on the display.</param>
		/// <param name="colums">The number of colums that will be shown at once.</param>
		/// <param name="rows">The number of rows that will be shown at once.</param>
		/// <param name="slotWidth">The width of the meu iten slots in pixels.</param>
		/// <param name="slotHeight">The height of the meu iten slots in pixels</param>
		Menu(uint8_t itemCount, int8_t posX, int8_t posY, uint8_t colums, uint8_t rows, uint8_t slotWidth, uint8_t slotHeight);
		~Menu();

		/// <summary>
		/// Adds a menu item to the menu.
		/// </summary>
		/// <param name="displayValue">String that should be shown.</param>
		/// <param name="command">ID of the command that should be executed, if the menu item gets confirmed.</param>
		void AddItem(char* displayValue, byte command);

		/// <summary>
		/// Renders the menu.
		/// </summary>
		/// <param name="display">The display the menu should get rendered to.</param>
		void Render(Adafruit_SSD1306* display);
		
		/// <summary>
		/// Moves the cursor right, if it is possible. If it was not possible, since the cursor was on the edge of the menu, the function returns false.
		/// </summary>
		bool CursorRight();

		/// <summary>
		/// Moves the cursor left, if it is possible. If it was not possible, since the cursor was on the edge of the menu, the function returns false.
		/// </summary>
		bool CursorLeft();

		/// <summary>
		/// Moves the cursor to the up, if it is possible. If it was not possible, since the cursor was on the edge of the menu, the function returns false.
		/// </summary>
		bool CursorUp();

		/// <summary>
		/// Moves the cursor to the down, if it is possible. If it was not possible, since the cursor was on the edge of the menu, the function returns false.
		/// </summary>
		bool CursorDown();

		/* Getter and setter */
		void SetPointer(uint8_t pointer);
		int GetPointer();
		MenuItem* GetSelectedItem();
		void SetShowPointer(bool state);
		void SetScrollbar(bool enabled);
		void SetMenuType(uint8_t type);
};


#endif

