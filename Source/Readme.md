# PasswordTyper source code

To get this running just create a new Arduino project and add the *.h and *.cpp files.  
You also have to install the Libraries for the display:  
https://github.com/adafruit/Adafruit_SSD1306  
Remember to configure the correct display in Adafruit_SSD1306.h. This should be active: #define SSD1306_128_64  instead of #define SSD1306_128_32  
https://github.com/adafruit/Adafruit-GFX-Library  

If you get it to work and it demands a PIN, just press the OK button. The standard PIN is empty.

## Keyboard layout
Currently, the PasswordTyper will act as a German keyboard. You can change that behavior by changing the mapping table named CHARS_US_TO_DE in the MainController.h file.  
For the US layout you can disable the mapping altogether by switching this:

    char* temp = StringHelper::GetCharArrayWithStop(pgm_read_byte(&CHARS_US_TO_DE[str[i]]));
	
to this:

	char* temp = StringHelper::GetCharArrayWithStop(str[i]);
	
in the KeyboardType(...) function in MainController.cpp

For other layouts you have to find the mapping on your own ;-)