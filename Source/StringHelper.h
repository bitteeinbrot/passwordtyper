#ifndef _STRINGHELPER_h
#define _STRINGHELPER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

class StringHelper {
	public:
		/// <summary>
		/// Returns the string as char array.
		/// </summary>
		/// <param name="str">The string.</param>
		static char* GetCharArrayFromString(String str);

		/// <summary>
		/// Returns the char within a char array, that ends with a strinf stop sign (\0).
		/// </summary>
		/// <param name="theChar">Char to be converted.</param>
		static char* GetCharArrayWithStop(char theChar);
		
		/// <summary>
		/// Fills the given string into the given char array.
		/// </summary>
		/// <param name="target">Char array the string should be filled in to.</param>
		/// <param name="targetBufferSize">Length of the char array.</param>
		/// <param name="value">String that should be inserted.</param>
		static void Fill(char* target, int targetBufferSize, String value);
		
		/// <summary>
		/// Initializes the given char array with \0
		/// </summary>
		/// <param name="string">Char array to initialize.</param>
		/// <param name="bufferLength">Length of the char array.</param>
		static void Initialize(char* string, int bufferLength);
		
		/// <summary>
		/// Inserts the given char into the char array at the given index.
		/// </summary>
		/// <param name="index">Index the char should be inserted. Can also be negative (-1 would insert the char at the last index).</param>
		/// <param name="target">Target char array.</param>
		/// <param name="targetBufferLegth">Size of the target char array.</param>
		/// <param name="value">Chart to be inserted.</param>
		static void InsertChar(int index, char* target, int targetBufferLegth, char value);
		
		/// <summary>
		/// Deletes the char at the given index from the given char array.
		/// </summary>
		/// <param name="index">Index the char should be deleted. Can also be negative (-1 would delete the char at the last index).</param>
		/// <param name="target">Target char array.</param>
		/// <param name="targetBufferLegth">Size of the target char array.</param>
		static void DeleteChar(int index, char* target, int targetBufferLegth);
		
		/// <summary>
		/// Returns the length of the given string inside the char array.
		/// </summary>
		/// <param name="string"></param>
		/// <param name="bufferLength">Size of the given char array.</param>
		static int Length(char* string, int bufferLength);
		
		/// <summary>
		/// Compares two strings. Returns false, if the don't match.
		/// </summary>
		/// <param name="string1">First string</param>
		/// <param name="string2">Second string</param>
		static bool Equals(char* string1, char* string2);
};

#endif

