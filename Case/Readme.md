# PasswordTyper case

## General

The case was made with [SketchUp](http://www.sketchup.com/). You can download and use SketchUp for free, as long as you don't use it commercially.

Maybe you have to adjust the cut for the display. I used two simmilar displays, but I had to move the cut 1mm up for the second one (of course, I realized it AFTER I 3D-printed it).

## How to print

You can print all parts without supports. I added built-in supports for the buttons that have to be cut like in the image below.

![](https://bitbucket.org/bitteeinbrot/passwordtyper/downloads/PasswordTyper_button_cut.jpg)

Make sure the button heads fit smoothly into the button holes of the lid. Maybe you have to sand them a little.
Also, at the bottom of the button is a hole that should be big enough to fit the (electronic?) button on the board. Make sure your slicer slices the buttons correctly. In Simplify3D I had to set "Only use perimeters for thin walls" in the advanced tab.

Make sure the lid fits, maybe you have to sand a little.