# PCB for PasswordTyper

The PCB was designed with ExpressPCB. You can download it [here](https://www.expresspcb.com/expresspcb/) for free.  
If you print the PCB, test the size! I had to scale it up a bit.