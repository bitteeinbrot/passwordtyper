# PasswordTyper

![](https://bitbucket.org/bitteeinbrot/passwordtyper/downloads/PasswordTyper_main_menu.JPG)

The PassworTyper is a little device that can type passwords for you. Just plug it in and it gets recognized as a keyboard. Add some passwords and you can select them to be entered.  
This device holds up to 30 passwords with the length of 20 chars (more weren't possible, since the EEPROM is too small for more). I could squeeze one or two more in, if I shorten the name of the password entries from 10 to 8 chars, but I figured 30 is enough for me.

Maybe I will develop a second version with a bigger micro controller for more entries and better security.

## Security

Since I used the ATmega32U4, there wasn't enough flash space to implement an encryption method. So, the passwords are held in plain text. If someone steals the device, he or she can extract the passwords from the EEPROM (some programming skills required).
But, to get to the passwords the standard way, you have to enter a PIN with 4096 possibilities first.  
Also, if someone put a key tracker onto your PC, tablet, whatever, passwords entered by the PasswordTyper get tracked, too.

## Build one

Things you need to build one:

* Arduino replica Pro Micro ATmega32U4 5V 16MHz
* 0.96" OLED display I2C 128x64px SSD1306 (the version with 4 pins)
* 7 x micro buttons 3x6x5mm (6 to control the menus and 1 for programming/reset)
* 6 x 1k-10k ohm resistors for pin pull down
* 5 x M2x2.5mm-5mm screws (the ones you use to fasten SSDs in your PC case). The screw head shouldn't be higher than 2mm, if you use my case design.
* a way to make the PCB
* a way to build the case (access to a 3D-printer)
* a way to upload the code to the ATmega32U4 (e.g. PC with Arduino IDE)

![](https://bitbucket.org/bitteeinbrot/passwordtyper/downloads/PasswordTyper_open_1.JPG)
![](https://bitbucket.org/bitteeinbrot/passwordtyper/downloads/PasswordTyper_open_2.JPG)

### Step 1 (prototype)

I would recommend to pre-assamble all electronic parts on a breadboard to see if you can get the code running. My ATmega32U4 acted strange at first, it ran very slow and lost its code after reboot until I flashed it with the Caterina-promicro16 bootloader from Sparkfun (https://github.com/sparkfun/Arduino_Boards/tree/master/sparkfun/avr/bootloaders/caterina).

Please orient yourself at the PCB diagramm below an remember: __the view is from BELOW__. Maybe I will publish a better diagramm later.

![](https://bitbucket.org/bitteeinbrot/passwordtyper/downloads/PasswordTyper_PCB.png)

Buttons (don't forget to pull them down with 1k-10k ohm resistors)

* Button left to pin 14
* Button right to pin 16
* Button up to pin 10
* Button down to pin 7
* Button back/cancel to pin 9
* Button ok to pin 8

Disaplay

* VCC to VCC
* GND to GND
* SCL to pin 3
* SDA to pin 2

### Step 2 (deploy the code)

Follow the instructions of the readme in the "Source" folder.

### Step 3 (make the PCB)

You can try to use a perfboard, but I guess you would have to make the case a bit bigger.  
To make the device as small as possible I decided to etch a board. It's not that hard and there are a ton of tutorials online. Just google "PCB etching". You can find the PCB layout in the folder "PCB"

I put the micro controller directly onto the PCB (without spacer or socket).  
There shouldn't be an air gap between buttons and board. The plastic of the buttons have to touch the PCB. This has to be this way for the 3D-printed buttons to fit.  
The cables I used to attach the display were about 4cm long.  
The rest is just "normal" soldering.

### Step 4 (print the case)

Follow the instructions of the readme in the "Case" folder.

### Step 5 (assamble)

* Put the display onto the display mount.
* Screw the display mount to the lid.
* Put the board into the case and close the lid.
* Check if the separator (the bar on the lid) is not too long, maybe you have to sand it down a little.
* Fasten the lid with a screw.
* Push in the buttons. They should snap in smoothly.
* Plug it in and enter your passwords!
